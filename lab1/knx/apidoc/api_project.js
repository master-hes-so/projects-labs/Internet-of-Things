define({
  "name": "knx server",
  "version": "0.1.0",
  "description": "knx REST server",
  "title": "knx REST server",
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2018-10-16T22:20:01.773Z",
    "url": "http://apidocjs.com",
    "version": "0.17.6"
  }
});
